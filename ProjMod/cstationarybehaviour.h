#ifndef CSTATIONARYBEHAVIOUR_H
#define CSTATIONARYBEHAVIOUR_H


class CStationaryBehaviour
{
public:
    CStationaryBehaviour();
    virtual void behaviourCheck(double x, double y, double dist);
};

#endif // CSTATIONARYBEHAVIOUR_H
