#include "carena.h"
#include <QDebug>
CArena::CArena()
{

}

CObject *CArena::addObjects(double X, double Y, UnitType type, Team team)
{
    CObject *obj;
    switch (type) {
    case jeep:
        obj = new CJeep(team,X,Y);
        break;

    case tank:
        obj = new CTank(team,X,Y);
        break;

    case cannon:
        obj = new CCannon(team,X,Y);
        break;
    }

    objectsList.push_back(obj);

    qDebug() << "dodane";
    return obj;


}



std::deque<CObject*> CArena::getObjectsList()
{
    return objectsList;
}

void CArena::Update()
{
    if (simulation == 1)
    {
        for (CObject *obj : objectsList)
        {
            CObject *act;
            act = obj->update(objectsList);
            if(act != NULL){
                objectsList.push_back(act);
            }

        }
       cleanObjectsList();
    }

}

void CArena::startWar()
{
    this->simulation = 1;
}

void CArena::stopWar()
{
    this->simulation = 0;
}

void CArena::cleanObjectsList()
{
    int i=0;
    for (CObject *obj : objectsList){
        if (CBullet *bullet = dynamic_cast <CBullet*>(obj)){
            if(bullet->isAlive()!=true)
            {
                objectsList.erase(objectsList.begin()+i);
                std::cout << bullet->isAlive()  << std::endl;
            }
        }
        i++;

    }


}

