#include "unitadd.h"
#include "ui_unitadd.h"


UnitAdd::UnitAdd(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UnitAdd)
{
    ui->setupUi(this);
    team = red;
    srand(time(NULL));
}

UnitAdd::~UnitAdd()
{
    delete ui;
}

void UnitAdd::setArenaPointer(CArena *wsk)
{
    arena = wsk;
}

void UnitAdd::setCanvasPointer(SfmlCanvas *wsk)
{
    canvas = wsk;
}

void UnitAdd::on_addButton_clicked()
{
    switch (ui->listWidget->currentRow()) {
    case 0:
        unitType = jeep;
        break;
    case 1:
        unitType = tank;
        break;
    case 2:
        unitType = cannon;
        break;

    }

    canvas->addUnits(arena->addObjects(ui->XspinBox->value(), ui->YspinBox->value(), unitType, team));
}

void UnitAdd::on_redRadioButton_clicked()
{
    team = red;
}

void UnitAdd::on_blueRradioButton_2_clicked()
{
    team = blue;
}

void UnitAdd::on_generateButton_clicked()
{

    int x,y;
    x = rand() % 1280;
    y = rand() % 720;

    ui->XspinBox->setValue(x);
    ui->YspinBox->setValue(y);

}
