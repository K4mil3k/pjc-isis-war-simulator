#include "cmoving.h"
#include "cmath"

CMoving::CMoving(int velocity, double xpos, double ypos, double area, double xprev, double yprev)
    :CObject(xpos,ypos,area,xprev,yprev)
{
    this->velocity = velocity;
}

void CMoving::goTo(double x, double y)
{
    xprev = xpos;
    yprev = ypos;
    auto distance = [](double x1, double y1, double x2, double y2)->double{return sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));};

    double  xStep = (x - xpos)/distance(xpos, ypos, x, y)*velocity/100;
    double yStep = (y - ypos)/distance(xpos, ypos, x, y)*velocity/100;

    xpos += xStep;
    ypos += yStep;
}

void CMoving::setVelocity(int V)
{
    velocity = V;
}


