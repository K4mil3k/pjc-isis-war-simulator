#include "cstationaryunit.h"

CStationaryUnit::CStationaryUnit(Team team, double xpos, double ypos, double area, int damage, int health, int reach, int reloadT, bool alive)
    : CUnit(damage,health,reach,reloadT,team,alive), CStationary(xpos,ypos,area)
{

}

CObject *CStationaryUnit::behaviourCheck(double x, double y, double dist, double xpos, double ypos, std::deque<CObject *> listOfObjects, int cyclesToFire, Team team, double damage)
{
    cyclesToFire = getcyclesToFire();
    if (dist <= reach)
    {
        if (cyclesToFire >= reloadT)
        {
            CObject *obj;

            obj = shoot(xpos,ypos,x,y,team,listOfObjects,damage);
            resetcyclesToFire();
            return obj;
        }
        else
        {
            return NULL;
        }

    }
    else if (dist > reach)
    {
        return NULL;
    }

}

