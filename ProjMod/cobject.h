#ifndef COBJECT_H
#define COBJECT_H
#include <deque>

/**
 * @brief The CObject class klasa opisujaca obiekty
 */
class CObject
{
public:
    /**
     * @brief CObject konstruktor klasy CObject
     * @param xpos aktualna wspolrzedna x obiektu
     * @param ypos aktualna wspolrzedna x obiektu
     * @param area promien okregu wyznaczajacego pole powierchni jednstki
     * @param xprev wspolrzedna x poprzedniej pozycji
     * @param yprev wspolrzedna y poprzedniej pozycji
     */
    CObject(double xpos, double ypos,double area,double xprev, double yprev);

    /**
     * @brief ~CObject destruktor klasy CObject
     */
    virtual ~CObject();

    /**
     * @brief getPositionX metoda zwracajaca wspolrzedna x obiektu
     * @return wspolrzedna x
     */
    double getPositionX();

    /**
     * @brief getPositionY metoda zwracajaca wspolrzedna y obiektu
     * @return wspolrzedna y
     */
    double getPositionY();

    /**
     * @brief getPositionXprev metoda zwracajaca poprzednia wspolrzedna x obiektu
     * @return poprzednia wspolrzedna x
     */
    double getPositionXprev();

    /**
     * @brief getPositionYprev metoda zwracajaca poprzednia wspolrzedna y obiektu
     * @return  poprzednia wspolrzedna x
     */
    double getPositionYprev();

    /**
     * @brief getArea metoda zwracajaca poprzednia promien okr pola powierzchni obiektu
     * @return
     */
    double getArea();

    /**
     * @brief update polimorficzna metoda modyfiklujaca parametry danego obiektu
     * oraz mogaca zwrocic nowy obiekt
     * @param listOfObjects lista obiektow na mapie
     * @return wskaznik do obiektu
     */
    virtual CObject *update(std::deque <CObject*> listOfObjects) = 0;


protected:
    double area; ///< pole powierzchni obiektu
    double xprev = 0; ///< poprzednia wspolrzedna x obiektu
    double yprev = 0; ///< poprzednia wspolrzedna y obiektu
    double xpos = 0; ///< wspolrzedna x obiektu
    double ypos = 0; ///< wspolrzedna x obiektu
private:


};

#endif // COBJECT_H
