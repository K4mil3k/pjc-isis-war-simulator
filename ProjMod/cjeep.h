#ifndef CJEEP_H
#define CJEEP_H
#include "cmoving.h"
#include "cunit.h"
#include <cmath>
#include <qdebug.h>
#include "cmovingunit.h"

/**
 * @brief The CJeep class klasa opisujaca ruchomy samochod terenowy
 */
class CJeep : public CMovingUnit
{
public:
    /**
     * @brief CJeep konstruktor klasy CJeep
     * @param team druzyna jednostki
     * @param xpos wspolrzedna x jednostki na mapie
     * @param ypos wspolrzedna y jednostki na mapie
     * @param area promien okregu wyznaczajacego pole powierchni jednstki
     * @param damage obrazenia zadawane przez jednostke
     * @param health punkty zycia jednostki
     * @param reach zasieg pola strzalu jednostki
     * @param reloadT czas przeladowania jednostki
     * @param velocity predkosc poruszania jednostki
     * @param alive pole, ktorego wartosc stwierdza czy jednostka ciagle istnieje na mapie
     */
    CJeep(Team team, double xpos, double ypos, double area = 15, int damage = 10, int health = 100, int reach = 150, int reloadT = 5, int velocity = 100, bool alive = 1);

    /**
     * @brief update polimorficzna metoda modyfikujaca parametry danego obiektu
     * oraz mogaca zwrocic nowy obiekt
     * @param listOfObjects lista obiektow na mapie
     * @return wskaznik do obiektu
     */
    CObject *update(std::deque <CObject*> listOfObjects) override;
protected:

private:

};

#endif // CJEEP_H
