#include "cobject.h"

CObject::CObject(double xpos, double ypos, double area, double xprev, double yprev)
{
    this->xpos = xpos;
    this->ypos = ypos;
    this->area = area;
    this->xprev = xprev;
    this->yprev = yprev;
}

CObject::~CObject()
{

}

double CObject::getPositionX()
{
    return xpos;
}

double CObject::getPositionY()
{
    return ypos;
}

double CObject::getPositionXprev()
{
    return xprev;
}

double CObject::getPositionYprev()
{
    return yprev;
}

double CObject::getArea()
{
    return area;
}





