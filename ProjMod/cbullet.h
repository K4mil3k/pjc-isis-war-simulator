#ifndef CBULLET_H
#define CBULLET_H
#include "cmoving.h"
#include "cunit.h"
#include "structures.h"
#include <iostream>

/**
 * @brief The CBullet class klasa opisujaca wystrzeliwane pociski
 */
class CBullet : public CMoving
{
public:
    /**
     * @brief CBullet konstruktor obiektu "pocisk"
     * @param xpos aktualna wspolrzedna x pocisku na mapie gry
     * @param ypos aktualna wspolrzedna y pocisku na mapie gry
     * @param xend koncowa wspolrzedna x ruchu pocisku na mapie gry
     * @param yend koncowa wspolrzedna y ruchu pocisku na mapie gry
     * @param team druzyna jednostki wystrzeliwujacej pocisk
     * @param damage obrazenia zadawane przez pocisk
     * @param alive pole, ktorego wartosc stwierdza czy pocisk ciagle istnieje na mapie
     * @param velocity predkosc pocisku
     * @param area promien pola razenia pocisku
     */
    CBullet(double xpos, double ypos,double xend, double yend, Team team, double damage, bool alive = 1, int velocity = 200, double area = 10);

    /**
     * @brief update polimorficzna metoda pocisku parametry danego obiektu
     * oraz mogaca zwrocic nowy obiekt
     * @param listOfObjects lista obiektow na mapie
     * @return wskaznik do obiektu
     */
    CObject *update(std::deque <CObject*> listOfObjects) override;

    /**
     * @brief isAlive metoda zwracajaca wartosc pola alive
     * @return wartosc pola alive
     */
    bool isAlive();

    /**
     * @brief checkHit metoda sprawdzajaca czy doszlo do kolizji pocisku z jednostka druzyny przeciwnej
     * @param listOfObjects lista obiektow na mapie
     * @param x aktualna wspolrzedna x pocisku na mapie gry
     * @param y aktualna wspolrzedna y pocisku na mapie gry
     * @param team druzyna jednostki wystrzeliwujacej pocisk
     * @param area promien pola razenia pocisku
     */
    void checkHit(std::deque <CObject*> listOfObjects, double x, double y, Team team, double area);

    /**
     * @brief goTo funkcja aktualizujaca wspolrzedne pocisku
     * @param x aktualna wspolrzedna x pocisku na mapie gry
     * @param y aktualna wspolrzedna y pocisku na mapie gry
     */
    void goTo(double x, double y) override;

protected:
double damage; ///< obrazenia zadawane przez pocisk
Team team; ///< druzyna jednostki wystrzeliwujacej pocisk
bool alive = 1; ///< pole, ktorego wartosc stwierdza czy pocisk ciagle istnieje na mapie
double xend, yend; /// < wspolrzende x oraz y wykrytej pozycji przeciwnika

private:
bool flag = 0; ///< pole, bedace flaga wysterowywana w momencie dotarcia pocisku do wykrytej pozycji przeciwnika
double xStep, yStep; ///< kroki inkrementacji aktualnych wspolrzednych x oraz y pocisku
};

#endif // CBULLET_H
