#include "cjeep.h"

CJeep::CJeep(Team team, double xpos, double ypos, double area, int damage, int health, int reach, int reloadT, int velocity,  bool alive)
    : CMovingUnit(team,xpos,ypos,area,xprev,yprev,damage,health,reach,reloadT,velocity,alive)
{

}

CObject *CJeep::update(std::deque<CObject *> listOfObjects)
{
    if(isAlive()!=0)
    {
        updatecyclesToFire();
        PointnDist nearestEnemy;
        CObject *obj;
        nearestEnemy = checkEnemies(listOfObjects,getPositionX(),getPositionY(),getTeam());
        obj = behaviourCheck(nearestEnemy.x,nearestEnemy.y,nearestEnemy.dist,getPositionX(),getPositionY(),listOfObjects,getcyclesToFire(),team,damage);
        return obj;

    }
}




