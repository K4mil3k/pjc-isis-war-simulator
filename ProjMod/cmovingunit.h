#ifndef CMOVINGUNIT_H
#define CMOVINGUNIT_H
#include "cmoving.h"
#include "cunit.h"
#include "iostream"
#include "structures.h"


/**
 * @brief The CMovingUnit class klasa opisujaca jednostki ruchome
 */
class CMovingUnit: public CMoving, public CUnit
{
public:
    /**
     * @brief CMovingUnit kosntruktor klasy CMovingUnit
     * @param team druzyna jednostki
     * @param xpos wspolrzedna x jednostki na mapie
     * @param ypos wspolrzedna y jednostki na mapie
     * @param area promien okregu wyznaczajacego pole powierchni jednstki
     * @param xprev wspolrzedna x poprzedniej pozycji
     * @param yprev wspolrzedna y poprzedniej pozycji
     * @param damage obrazenia zadawane przz jednostke
     * @param health punkty zycia jednostki
     * @param reach zasieg pola widzenia jednostki
     * @param reloadT czas przeladowania jednostki
     * @param velocity predkosc poruszania jednostki
     * @param alive pole, ktorego wartosc stwierdza czy jednostka ciagle istnieje na mapie
     */
    CMovingUnit(Team team, double xpos, double ypos,double area, double xprev,double yprev ,int damage,int health, int reach, int reloadT, int velocity, bool alive);

    /**
     * @brief behaviourCheck metoda sprawdzajaca zachowanie jednostki ruchomej
     * @param x wspolrzedna x wykrytej przeszkody
     * @param y wspolrzedna y wykrytej przeszkody
     * @param dist odleglosc od przeszkody
     * @param xpos aktualna wspolrzedna x jednostki
     * @param ypos aktualna wspolrzedna y jednostki
     * @param listOfObjects lista obiektow
     * @param cyclesToFire liczba cykli od wystrzalu
     * @param team druzyna jednsotki
     * @param damage zadawane obrazenia
     * @return obiekt
     */
    CObject *behaviourCheck(double x, double y, double dist,double xpos,double ypos, std::deque <CObject*> listOfObjects, int cyclesToFire,Team team,double damage);

    /**
     * @brief update polimorficzna metoda modyfikujaca parametry danego obiektu
     * oraz mogaca zwrocic nowy obiekt
     * @param listOfObjects lista obiektow na mapie
     * @return wskaznik do obiektu
     */
    virtual CObject *update(std::deque <CObject*> listOfObjects) = 0;
};

#endif // CMOVINGUNIT_H
