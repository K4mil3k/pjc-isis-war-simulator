#include "cmovingunit.h"

CMovingUnit::CMovingUnit(Team team, double xpos, double ypos, double area, double xprev, double yprev, int damage, int health, int reach, int reloadT, int velocity, bool alive)
    : CUnit(damage,health,reach,reloadT,team,alive), CMoving(velocity,xpos,ypos,area,xprev,yprev)
{

}


CObject *CMovingUnit::behaviourCheck(double x, double y, double dist, double xpos, double ypos, std::deque<CObject *> listOfObjects, int cyclesToFire, Team team, double damage)
{
    cyclesToFire = getcyclesToFire();
    if (dist <= reach)
    {
        if (cyclesToFire >= reloadT)
        {
            CObject *obj;

            obj = shoot(xpos,ypos,x,y,team,listOfObjects,damage);
            resetcyclesToFire();
            return obj;
        }
        else
        {
            return NULL;
        }

    }
    else if (dist > reach)
    {
        if ((xpos > x-2 && xpos < x+2) && (ypos > y-2 && ypos < y+2))
        {

        }
        else
        {
            goTo(x,y);
        }

        return NULL;
    }
}


