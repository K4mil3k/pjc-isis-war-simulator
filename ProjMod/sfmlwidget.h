#ifndef SFMLWIDGET_H
#define SFMLWIDGET_H


#ifdef Q_WS_X11
#include <Qt/qx11info_x11.h>
#include <X11\Xlib.h>
#endif


#include <QTimer>
#include <QObject>
#include <QWidget>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

/**
 * @brief The SfmlWidget class klasa okna SFML
 */

class SfmlWidget : public QWidget, public sf::RenderWindow
{
    Q_OBJECT
public:
    explicit SfmlWidget(QWidget *parent = 0);

    virtual QPaintEngine *paintEngine() const;
    virtual void showEvent(QShowEvent *event);
    virtual void paintEvent(QPaintEvent *event);
    virtual void onInit();
    virtual void onUpdate();


private:
    QTimer m_timer;
    bool m_initialized;
};

#endif // SFMLWIDGET_H
