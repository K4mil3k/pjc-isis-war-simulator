/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "sfmlcanvas.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    SfmlCanvas *widget;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *AddUnit;
    QPushButton *WarButton;
    QPushButton *PeaceButton;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1437, 799);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        widget = new SfmlCanvas(centralWidget);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(10, 20, 1280, 720));
        widget->setMinimumSize(QSize(1280, 720));
        widget->setMaximumSize(QSize(1279, 720));
        widget->setMouseTracking(false);
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(1310, 30, 101, 251));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        AddUnit = new QPushButton(layoutWidget);
        AddUnit->setObjectName(QStringLiteral("AddUnit"));

        verticalLayout->addWidget(AddUnit);

        WarButton = new QPushButton(layoutWidget);
        WarButton->setObjectName(QStringLiteral("WarButton"));

        verticalLayout->addWidget(WarButton);

        PeaceButton = new QPushButton(layoutWidget);
        PeaceButton->setObjectName(QStringLiteral("PeaceButton"));

        verticalLayout->addWidget(PeaceButton);

        MainWindow->setCentralWidget(centralWidget);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "ISIS: Simulator", Q_NULLPTR));
        AddUnit->setText(QApplication::translate("MainWindow", "Add units", Q_NULLPTR));
        WarButton->setText(QApplication::translate("MainWindow", "WAR!!!!!!", Q_NULLPTR));
        PeaceButton->setText(QApplication::translate("MainWindow", "Peace", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
