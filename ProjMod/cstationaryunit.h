#ifndef CSTATIONARYUNIT_H
#define CSTATIONARYUNIT_H
#include "cstationary.h"
#include "cunit.h"
#include "structures.h"



/**
 * @brief The CStationaryUnit class klasa opisujaca jednostki nieruchome
 */
class CStationaryUnit: public CStationary, public CUnit
{
public:
    /**
     * @brief CStationaryUnit kosntruktor klasy CStationaryUnit
     * @param team druzyna jednostki
     * @param xpos wspolrzedna x jednostki na mapie
     * @param ypos wspolrzedna y jednostki na mapie
     * @param area promien okregu wyznaczajacego pole powierchni jednstki
     * @param damage obrazenia zadawane przz jednostke
     * @param health punkty zycia jednostki
     * @param reach zasieg pola widzenia jednostki
     * @param reloadT czas przeloadowania jednostki
     * @param alive pole, ktorego wartosc stwierdza czy jednostka ciagle istnieje na mapie
     */
    CStationaryUnit(Team team, double xpos, double ypos, double area,int damage,int health, int reach, int reloadT, bool alive);

    /**
     * @brief behaviourCheck metoda sprawdzajaca zachowanie jednostki nieruchomej
     * @param x wspolrzedna x wykrytej przeszkody
     * @param y wspolrzedna y wykrytej przeszkody
     * @param dist odleglosc od przeszkody
     * @param xpos aktualna wspolrzedna x jednostki
     * @param ypos aktualna wspolrzedna y jednostki
     * @param listOfObjects lista obiektow
     * @param cyclesToFire liczba cykli od wystrzalu
     * @param team druzyna jednsotki
     * @param damage zadawane obrazenia
     * @return obiekt (pocisk)
     */
    CObject *behaviourCheck(double x, double y, double dist,double xpos,double ypos, std::deque <CObject*> listOfObjects, int cyclesToFire,Team team,double damage);

    /**
     * @brief update polimorficzna metoda modyfiklujaca parametry danego obiektu
     * oraz mogaca zwrocic nowy obiekt
     * @param listOfObjects lista obiektow na mapie
     * @return wskaznik do obiektu
     */
    virtual CObject *update(std::deque <CObject*> listOfObjects) = 0;
};

#endif // CSTATIONARYUNIT_H
