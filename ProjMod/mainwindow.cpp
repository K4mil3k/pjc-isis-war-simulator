#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    arena = new CArena();
    ui->setupUi(this);
    canvas = ui->widget;
    timer = new QTimer(this);

    connect(timer, SIGNAL(timeout()), this, SLOT(timerFunction()));
    timer->start(17);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::timerFunction()
{
    arena->Update();
    canvas->refreshlistOfObjectsCanvas(arena->getObjectsList());
}

void MainWindow::on_AddUnit_clicked()
{
    UnitAdd add;
    add.setModal(true);
    add.setArenaPointer(arena);
    add.setCanvasPointer(canvas);
    add.exec();
}

void MainWindow::on_WarButton_clicked()
{
    arena->startWar();
}


void MainWindow::on_PeaceButton_clicked()
{
    arena->stopWar();
}
