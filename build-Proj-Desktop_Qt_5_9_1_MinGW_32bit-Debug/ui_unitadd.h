/********************************************************************************
** Form generated from reading UI file 'unitadd.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UNITADD_H
#define UI_UNITADD_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpinBox>

QT_BEGIN_NAMESPACE

class Ui_UnitAdd
{
public:
    QListWidget *listWidget;
    QSpinBox *XspinBox;
    QSpinBox *YspinBox;
    QLabel *label;
    QLabel *label_2;
    QRadioButton *redRadioButton;
    QRadioButton *blueRradioButton_2;
    QPushButton *addButton;

    void setupUi(QDialog *UnitAdd)
    {
        if (UnitAdd->objectName().isEmpty())
            UnitAdd->setObjectName(QStringLiteral("UnitAdd"));
        UnitAdd->resize(400, 300);
        listWidget = new QListWidget(UnitAdd);
        new QListWidgetItem(listWidget);
        new QListWidgetItem(listWidget);
        new QListWidgetItem(listWidget);
        new QListWidgetItem(listWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(10, 10, 256, 192));
        XspinBox = new QSpinBox(UnitAdd);
        XspinBox->setObjectName(QStringLiteral("XspinBox"));
        XspinBox->setGeometry(QRect(280, 30, 61, 22));
        XspinBox->setMaximum(1280);
        YspinBox = new QSpinBox(UnitAdd);
        YspinBox->setObjectName(QStringLiteral("YspinBox"));
        YspinBox->setGeometry(QRect(280, 60, 61, 22));
        YspinBox->setMaximum(720);
        label = new QLabel(UnitAdd);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(350, 30, 41, 16));
        label_2 = new QLabel(UnitAdd);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(350, 60, 31, 16));
        redRadioButton = new QRadioButton(UnitAdd);
        redRadioButton->setObjectName(QStringLiteral("redRadioButton"));
        redRadioButton->setGeometry(QRect(280, 120, 95, 20));
        redRadioButton->setChecked(true);
        blueRradioButton_2 = new QRadioButton(UnitAdd);
        blueRradioButton_2->setObjectName(QStringLiteral("blueRradioButton_2"));
        blueRradioButton_2->setGeometry(QRect(280, 160, 95, 20));
        addButton = new QPushButton(UnitAdd);
        addButton->setObjectName(QStringLiteral("addButton"));
        addButton->setGeometry(QRect(90, 230, 93, 28));

        retranslateUi(UnitAdd);

        QMetaObject::connectSlotsByName(UnitAdd);
    } // setupUi

    void retranslateUi(QDialog *UnitAdd)
    {
        UnitAdd->setWindowTitle(QApplication::translate("UnitAdd", "Dialog", Q_NULLPTR));

        const bool __sortingEnabled = listWidget->isSortingEnabled();
        listWidget->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem = listWidget->item(0);
        ___qlistwidgetitem->setText(QApplication::translate("UnitAdd", "Jeep", Q_NULLPTR));
        QListWidgetItem *___qlistwidgetitem1 = listWidget->item(1);
        ___qlistwidgetitem1->setText(QApplication::translate("UnitAdd", "Tank", Q_NULLPTR));
        QListWidgetItem *___qlistwidgetitem2 = listWidget->item(2);
        ___qlistwidgetitem2->setText(QApplication::translate("UnitAdd", "Soldier", Q_NULLPTR));
        QListWidgetItem *___qlistwidgetitem3 = listWidget->item(3);
        ___qlistwidgetitem3->setText(QApplication::translate("UnitAdd", "Cannon", Q_NULLPTR));
        listWidget->setSortingEnabled(__sortingEnabled);

        label->setText(QApplication::translate("UnitAdd", "X pos", Q_NULLPTR));
        label_2->setText(QApplication::translate("UnitAdd", "Y pos", Q_NULLPTR));
        redRadioButton->setText(QApplication::translate("UnitAdd", "RED team", Q_NULLPTR));
        blueRradioButton_2->setText(QApplication::translate("UnitAdd", "BLUE team", Q_NULLPTR));
        addButton->setText(QApplication::translate("UnitAdd", "Add unit", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class UnitAdd: public Ui_UnitAdd {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UNITADD_H
