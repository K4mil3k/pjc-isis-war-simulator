#ifndef CRUCHOME_H
#define CRUCHOME_H
#include "cobject.h"
#include <cmath>


/**
 * @brief The CMoving class klasa opisujaca obiklety poruszajace sie
 */
class CMoving : public CObject

{
public:
    /**
     * @brief CMoving konstruktor klasy CMoving
     * @param velocity predkosc poruszania jednostki
     * @param xpos wspolrzedna x jednostki na mapie
     * @param ypos wspolrzedna y jednostki na mapie
     * @param area promien okregu wyznaczajacego pole powierchni jednstki
     * @param xprev poprzednia wspolrzedna x obiektu
     * @param yprev poprzednia wspolrzedna y obiektu
     */
    CMoving(int velocity, double xpos, double ypos,double area, double xprev, double yprev);

    /**
     * @brief goTo metoda aktualizujaca pozycje obiektu
     * @param x docelowa wspolrzedna x obiektu
     * @param y docelowa wspolrzedna x obiektu
     */
    virtual void goTo(double x, double y);

    /**
     * @brief setVelocity metoda ustawiajaca predkosc obiektu
     * @param V predkosc obiektu
     */
    void setVelocity(int V);

    /**
     * @brief update polimorficzna metoda modyfiklujaca parametry danego obiektu
     * oraz mogaca zwrocic nowy obiekt
     * @param listOfObjects lista obiektow na mapie
     * @return wskaznik do obiektu
     */
    virtual CObject *update(std::deque <CObject*> listOfObjects) = 0;

protected:
    int velocity; ///< pole predkosci
private:

};

#endif // CRUCHOME_H
