#ifndef CTANK_H
#define CTANK_H
#include "cmoving.h"
#include "cunit.h"
#include <cmath>
#include <qdebug.h>
#include "cmovingunit.h"

/**
 * @brief The CTank class klasa opisujaca ruchomy czolg
 */
class CTank : public CMovingUnit
{
public:
    /**
     * @brief CJeep konstruktor klasy CTank
     * @param team druzyna jednostki
     * @param xpos wspolrzedna x jednostki na mapie
     * @param ypos wspolrzedna y jednostki na mapie
     * @param area promien okregu wyznaczajacego pole powierchni jednstki
     * @param damage obrazenia zadawane przz jednostke
     * @param health punkty zycia jednostki
     * @param reach zasieg pola widzenia jednostki
     * @param reloadT czas przeladowania jednostki
     * @param velocity predkosc poruszania jednostki
     * @param alive pole, ktorego wartosc stwierdza czy jednostka ciagle istnieje na mapie
     */
    CTank(Team team, double xpos, double ypos, double area = 20, int damage = 50, int health = 300, int reach = 250, int reloadT = 200, int velocity = 40, bool alive = 1);

    /**
     * @brief update polimorficzna metoda modyfikujaca parametry danego obiektu
     * oraz mogaca zwrocic nowy obiekt
     * @param listOfObjects lista obiektow na mapie
     * @return wskaznik do obiektu
     */
    CObject *update(std::deque <CObject*> listOfObjects) override;
};

#endif // CTANK_H
