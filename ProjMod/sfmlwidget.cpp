#include "sfmlwidget.h"


SfmlWidget::SfmlWidget(QWidget *parent) : QWidget(parent), m_initialized(false)
{
    setAttribute(Qt::WA_PaintOnScreen);
    setAttribute(Qt::WA_OpaquePaintEvent);
    setAttribute(Qt::WA_NoSystemBackground);

    setFocusPolicy(Qt::StrongFocus);
}

QPaintEngine *SfmlWidget::paintEngine() const
{
    return NULL;
}

void SfmlWidget::showEvent(QShowEvent *event)
{
#ifdef Q_WS_X11
    XFlush(QX11info::display());
#endif
    if(m_initialized == false)
    {
        sf::RenderWindow::create(reinterpret_cast<sf::WindowHandle>(winId()));

        onInit();

        connect(&m_timer, SIGNAL(timeout()), this, SLOT(repaint()));

        m_timer.start();
        m_initialized = true;
    }
}

void SfmlWidget::paintEvent(QPaintEvent *event)
{
    RenderWindow::clear();
    onUpdate();
    RenderWindow::display();
}

void SfmlWidget::onInit()
{

}

void SfmlWidget::onUpdate()
{

}
