#include "cbullet.h"


CBullet::CBullet(double xpos, double ypos, double xend, double yend, Team team, double damage, bool alive, int velocity, double area)
    : CMoving(velocity,xpos,ypos,area,xprev,yprev)
{
    this->xend = xend;
    this->yend = yend;
    this->team = team;
    this->damage = damage;
}

CObject *CBullet::update(std::deque<CObject *> listOfObjects)
{
    if(isAlive()!=0)
    {
        checkHit(listOfObjects,xpos,ypos,team,area);
        goTo(xend,yend);

        if((xpos >= 1280 && xpos <= 0) && (ypos >= 720 && ypos <= 0))
        {
            this->alive=0;
        }
    }

    return NULL;

}

bool CBullet::isAlive()
{
    return alive;
}

void CBullet::checkHit(std::deque<CObject *> listOfObjects, double x, double y, Team team, double area)
{

    double dist = 0;

    auto distance = [](double x1, double y1, double x2, double y2)->double{return sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));};

    for (CObject *obj : listOfObjects){
        if(CUnit *unit = dynamic_cast <CUnit*>(obj)){
            if (team != unit->getTeam()&&(unit->isAlive()==1)){
                dist = distance(obj->getPositionX(),obj->getPositionY(),x,y);
                if (dist <= area+obj->getArea()){
                    unit->reduceHealth(damage);
                    alive = 0;
                }
            }
        }
    }

}

void CBullet::goTo(double x, double y)
{
    auto distance = [](double x1, double y1, double x2, double y2)->double{return sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));};
    if (flag == 0)
    {
        xStep = (x - xpos)/distance(xpos, ypos, x, y)*velocity/100;
        yStep = (y - ypos)/distance(xpos, ypos, x, y)*velocity/100;
        flag = 1;
    }

    xpos += xStep;
    ypos += yStep;


}
