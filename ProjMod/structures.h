#ifndef STRUCTURES_H
#define STRUCTURES_H

/**
 * struktury wykorzystywane w projekcie
 */

/**
 * @brief The UnitType enum struktura przechowujaca typ jednostki
 */
enum UnitType
{
    jeep = 0,
    tank = 1,
    cannon = 2
};

/**
 * @brief The Team enum struktura przechowujaca druzyne
 */
enum Team
{
    red = 0,
    blue = 1
};

/**
 * @brief The BulletType enum struktura przechowujaca typ pocisku
 */
enum BulletType
{
    bullet = 0,
};

/**
 * @brief The PointnDist struct struktura przechowujaca koordynaty oraz dystans
 */
struct PointnDist
{
    double x;
    double y;
    double dist;
};











#endif // STRUCTURES_H
