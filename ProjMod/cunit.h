#ifndef CUNIT_H
#define CUNIT_H
#include "structures.h"
#include <deque>
#include <cmath>
#include "cobject.h"
#include "cbullet.h"
#include <iostream>


/**
 * @brief The CUnit class klasa opisujaca jednostke
 */

class CUnit
{
public:
    /**
     * @brief CUnit konstruktor klasy CUnit
     * @param damage obrazenia zadawane przez jednostke
     * @param health punkty zycia jednostki
     * @param reach zasieg pola strzalu jednostki
     * @param reloadT czas przeladowania jednostki
     * @param team druzyna jednostki
     * @param alive pole, ktorego wartosc stwierdza czy jednostka ciagle istnieje na mapie
     */
    CUnit(int damage,int health, int reach, int reloadT, Team team, bool alive = 1);

    /**
     * @brief shoot metoda tworzaca pocisk
     * @param xpos wspolrzedna poczatkowa x pocisku
     * @param ypos wspolrzedna poczatkowa y pocisku
     * @param xen wspolrzedna koncowa x pocisku
     * @param yen wspolrzedna koncowa y pocisku
     * @param team druzyna jednostki wystrzeliwujacej pocisk
     * @param listOfObjects lista obiektów
     * @param damage obrazenia zadawane przez wystrzelony przez jednostke pocisk
     * @return wskaznik do obiektu
     */
    CObject *shoot(double xpos, double ypos, double xen, double yen, Team team,std::deque <CObject*> listOfObjects,double damage);

    /**
     * @brief checkEnemies metoda sprawdzajaca polozenie przeciwnikow
     * @param listOfObjects lista obiektow
     * @param x wspolrzedna x jednostki
     * @param y wspolrzedna y jednostki
     * @param team druzyna jednostki
     * @return koordynaty + dystans
     */
    PointnDist checkEnemies(std::deque<CObject *> listOfObjects, double x, double y, Team team);

    /**
     * @brief getTeam metoda pobierajaca druzyne
     * @return druzyna
     */
    Team getTeam();

    /**
     * @brief getDamage metoda pobierajaca zadawane obrazenia
     * @return obrazenia
     */
    int getDamage();

    /**
     * @brief addBullets metoda tworzaca nowy pocisk oraz zwracajaca wskaznik do niego
     * @param xpos wspolrzedna x pozycji poczatkowej pocisku
     * @param ypos wspolrzedna y pozycji poczatkowej pocisku
     * @param xend wspolrzedna x pozycji koncowej pocisku
     * @param yend wspolrzedna y pozycji koncowej pocisku
     * @param type typ pocisku
     * @param team druzyna jednostki wystrzliwujacej pocisk
     * @param listOfObjects lista obiektow
     * @param damage obrazenia zadawane przez pocisk
     * @return wskaznik do obiektu
     */
    CObject *addBullets(double xpos, double ypos, double xend, double yend, BulletType type, Team team, std::deque <CObject*> listOfObjects,double damage);

    /**
     * @brief isAlive metoda zwracajaca wartosc pola alive
     * @return alive
     */
    bool isAlive();

    /**
     * @brief updatecyclesToFire metoda aktualizujaca liczbe cykli od wystrzalu
     */
    void updatecyclesToFire();

    /**
     * @brief getcyclesToFire metoda pobierajaca liczbe cykli od wystrzalu
     * @return liczba cykli od wystrzalu
     */
    int getcyclesToFire();

    /**
     * @brief resetcyclesToFire metoda zerujaca liczbe cykli od wystrzalu
     */
    void resetcyclesToFire();

    /**
     * @brief reduceHealth metoda obnizajaca punkty zdrowa o wartosc zadanych obrazen
     * @param damage zadane obrazenia
     */
    void reduceHealth(double damage);

    int getHealth()
    {
        return health;
    }



protected:
    int damage, health, reach, reloadT; ///< pola zadawanych obrazen, punktow zdrowia, zasiegu oraz czasu przeladowania
    bool alive; ///< pole alive
    Team team; ///< pole druzyny
    int cyclesToFire = 0; ///< pole lczby cykli od wystrzalu

private:


};

#endif // CUNIT_H
