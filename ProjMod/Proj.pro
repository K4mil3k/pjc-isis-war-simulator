#-------------------------------------------------
#
# Project created by QtCreator 2017-05-29T23:39:27
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Proj
TEMPLATE = app

INCLUDEPATH += C:\Users\Kamil\Desktop\C++\SFML-2.4.2\include
DEPENDPATH += C:\Users\Kamil\Desktop\C++\SFML-2.4.2\include

LIBS += -LC:\Users\Kamil\Desktop\C++\SFML-2.4.2\lib\

CONFIG(debug, debug|release): LIBS += -lsfml-audio-d -lsfml-system-d -lsfml-graphics-d -lsfml-network-d -lsfml-window-d
CONFIG(release, debug|release): LIBS += -lsfml-audio -lsfml-system -lsfml-graphics -lsfml-network -lsfml-window

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        mainwindow.cpp \
    cunit.cpp \
    cmoving.cpp \
    cobject.cpp \
    cstationary.cpp \
    carena.cpp \
    cjeep.cpp \
    ctank.cpp \
    ccannon.cpp \
    unitadd.cpp \
    sfmlcanvas.cpp \
    sfmlwidget.cpp \
    cmovingunit.cpp \
    cbullet.cpp \
    cstationaryunit.cpp

HEADERS  += \
    cunit.h \
    cmoving.h \
    cobject.h \
    cstationary.h \
    carena.h \
    structures.h \
    cjeep.h \
    ctank.h \
    ccannon.h \
    unitadd.h \
    sfmlcanvas.h \
    sfmlwidget.h \
    cmovingunit.h \
    cbullet.h \
    cstationaryunit.h \
    mainwindow.h

FORMS    += mainwindow.ui \
    unitadd.ui
