#include "ccannon.h"

CCannon::CCannon(Team team, double xpos, double ypos, double area, int damage, int health, int reach, int reloadT, bool alive)
    :CStationaryUnit(team,xpos,ypos,area,damage,health,reach,reloadT,alive)
{

}

CObject *CCannon::update(std::deque<CObject *> listOfObjects)
{
    if(isAlive()!=0)
    {
        updatecyclesToFire();
        PointnDist nearestEnemy;
        CObject *obj;
        nearestEnemy = checkEnemies(listOfObjects,getPositionX(),getPositionY(),getTeam());
        obj = behaviourCheck(nearestEnemy.x,nearestEnemy.y,nearestEnemy.dist,getPositionX(),getPositionY(),listOfObjects,getcyclesToFire(),team,damage);
        return obj;

    }

}


