#include "sfmlcanvas.h"
#include <iostream>

SfmlCanvas::SfmlCanvas(QWidget *Parent) : SfmlWidget(Parent)
{

}

void SfmlCanvas::onInit()
{
    if(!background.loadFromFile("grass.jpg"))
    {
        std::cout << "Loading background failed" << std::endl;
    }

    if(!jeepTextureRED.loadFromFile("jeepRED.png"))
    {
        std::cout << "Loading Jeep texture failed" << std::endl;
    }

    if(!jeepTextureBLUE.loadFromFile("jeepBLUE.png"))
    {
        std::cout << "Loading Jeep texture failed" << std::endl;
    }

    if(!bulletTexture.loadFromFile("bullet.png"))
    {
        std::cout << "Loading Bullet texture failed" << std::endl;
    }

    if(!fireTexture.loadFromFile("flame.png"))
    {
        std::cout << "Loading Fire texture failed" << std::endl;
    }

    if(!tankTextureBLUE.loadFromFile("tankBLUE.png"))
    {
        std::cout << "Loading Tank texture failed" << std::endl;
    }

    if(!tankTextureRED.loadFromFile("tankRED.png"))
    {
        std::cout << "Loading Tank texture failed" << std::endl;
    }

    if(!cannonTextureBLUE.loadFromFile("cannonBLUE.png"))
    {
        std::cout << "Loading Cannon texture failed" << std::endl;
    }

    if(!cannonTextureRED.loadFromFile("cannonRED.png"))
    {
        std::cout << "Loading Cannon texture failed" << std::endl;
    }

    if(!font.loadFromFile("arial.ttf"))
    {
        std::cout << "Loading font failed" << std::endl;
    }
    else
    {
        text.setFont(font);
    }




}

void SfmlCanvas::onUpdate()
{
    sf::Sprite background1(background);
    sf::RenderWindow::draw(background1);
    int hp;

    for (CObject *object : listOfObjectsCanvas)
    {
        double x, y, xprev, yprev;
        if (object != NULL)
        {
            x = object->getPositionX();
            y = object->getPositionY();
            xprev = object->getPositionXprev();
            yprev = object->getPositionYprev();

            if (x-xprev > 0){
                angle = ((atan((y-yprev)/(x-xprev)))*180/3.14)+90;}
            if (x-xprev < 0){
                angle = ((atan((y-yprev)/(x-xprev)))*180/3.14)-90;}
            if (x-xprev == 0){
                angle = angleprev;
            }

            sf::Sprite sprite;
            if(CUnit *unit = dynamic_cast <CUnit*> (object))
            {
                if(unit->isAlive()!=0)
                {
                    if(CJeep *jeep = dynamic_cast <CJeep*>(object))
                    {
                        {if (jeep->getTeam() == 0){
                                sprite.setTexture(jeepTextureRED);
                                sprite.setOrigin(62.5,112);
                                sprite.setScale(sf::Vector2f(0.3f,0.3f));

                            }
                            else if (jeep->getTeam() == 1){
                                sprite.setTexture(jeepTextureBLUE);
                                sprite.setOrigin(62.5,112);
                                sprite.setScale(sf::Vector2f(0.3f,0.3f));

                            }

                            sprite.setPosition(x,y);
                            angleprev = angle;
                            sprite.setRotation(angle);


                            hp = jeep->getHealth();
                            std::string s = std::to_string(hp);

                            text.setString(s);
                            text.setCharacterSize(24);
                            text.setPosition(x-50,y-50);




                        }
                    }

                    if(CTank *tank = dynamic_cast <CTank*>(object))
                    {
                        if (tank->getTeam() == 0){
                            sprite.setTexture(tankTextureRED);
                            sprite.setOrigin(120,156);
                            sprite.setScale(sf::Vector2f(0.3f,0.3f));

                        }
                        else if (tank->getTeam() == 1){
                            sprite.setTexture(tankTextureBLUE);
                            sprite.setOrigin(119,163);
                            sprite.setScale(sf::Vector2f(0.3f,0.3f));

                        }

                        sprite.setPosition(x,y);
                        angleprev = angle;
                        sprite.setRotation(angle);

                        hp = tank->getHealth();
                        std::string s = std::to_string(hp);

                        text.setString(s);
                        text.setCharacterSize(24);
                        text.setPosition(x-50,y-50);
                    }

                    if(CCannon *cannon = dynamic_cast <CCannon*>(object))
                    {
                        if (cannon->getTeam() == 0){
                            sprite.setTexture(cannonTextureRED);
                            sprite.setOrigin(211,208);
                            sprite.setScale(sf::Vector2f(0.2f,0.2f));

                        }
                        else if (cannon->getTeam() == 1){
                            sprite.setTexture(cannonTextureBLUE);
                            sprite.setOrigin(211,208);
                            sprite.setScale(sf::Vector2f(0.2f,0.2f));

                        }

                        sprite.setPosition(x,y);
                        angleprev = angle;
                        sprite.setRotation(angle);

                        hp = cannon->getHealth();
                        std::string s = std::to_string(hp);

                        text.setString(s);
                        text.setCharacterSize(24);
                        text.setPosition(x-50,y-50);


                    }}
                else
                {
                    hp = 0;
                    std::string s = std::to_string(hp);

                    text.setString(s);
                    text.setCharacterSize(24);
                    text.setPosition(x-50,y-50);

                    sprite.setTexture(fireTexture);
                    sprite.setOrigin(426, 700.5);
                    sprite.setScale(sf::Vector2f(0.05f,0.05f));
                    sprite.setPosition(x,y);
                }

            }


            if (CBullet *bullet = dynamic_cast <CBullet*> (object))
            {
                if (bullet->isAlive()!=0)
                {
                    sprite.setTexture(bulletTexture);
                    sprite.setScale(sf::Vector2f(0.2f,0.2f));
                    sprite.setPosition(x-12,y-12);
                }

            }



            sf::RenderWindow::draw(sprite);
            sf::RenderWindow::draw(text);
        }

    }
}

void SfmlCanvas::addUnits(CObject *object){
    listOfObjectsCanvas.push_back(object);
}

void SfmlCanvas::setArenaPointer(CArena *wsk)
{
    arena = wsk;
}


void SfmlCanvas::refreshlistOfObjectsCanvas(std::deque<CObject *> listOfObjectsCanvas)
{
    this->listOfObjectsCanvas=listOfObjectsCanvas;
}


