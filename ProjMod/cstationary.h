#ifndef CSTATIONARY_H
#define CSTATIONARY_H
#include "cobject.h"

/**
 * @brief The CStationary class klasa opisujaca obiekty nieruchomme
 */
class CStationary: public CObject
{
public:
    /**
     * @brief CStationary konstruktor klasy CStationary
     * @param xpos wspolrzedna x obiektu
     * @param ypos wspolrzedna y obiektu
     * @param area promien okregu wyznaczajacego pole powierchni obiektu
     */
    CStationary(double xpos, double ypos, double area);

    /**
     * @brief update polimorficzna metoda modyfiklujaca parametry danego obiektu
     * oraz mogaca zwrocic nowy obiekt
     * @param listOfObjects lista obiektow na mapie
     * @return wskaznik do obiektu
     */
    virtual CObject *update(std::deque <CObject*> listOfObjects) = 0;

protected:

private:


};

#endif // CSTATIONARY_H
