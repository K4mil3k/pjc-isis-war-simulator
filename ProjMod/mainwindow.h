#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "unitadd.h"
#include "sfmlcanvas.h"
#include "QTimer"
#include "QDebug"

#include <QMainWindow>

/**
 * glowny formularz aplikacji
 */

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QTimer *timer; ///< timer
    CArena *arena;
    SfmlCanvas *canvas;


private slots:

    /**
     * @brief timerFunction metoda wyzwalajaca funkcje update oraz
     * odswierzanie listy rysowanych obiektow
     */
    void timerFunction();

    /**
     * @brief on_AddUnit_clicked metoda otwierajaca okno dodawania jednostek
     */
    void on_AddUnit_clicked();

    /**
     * @brief on_WarButton_clicked metoda wywolujaca ustawienie parametru simulation na wartosc 1
     */
    void on_WarButton_clicked();

    /**
     * @brief on_PeaceButton_clicked metoda wywalujaca ustawienie parametru simulation na wartosc 0
     */
    void on_PeaceButton_clicked();
};

#endif // MAINWINDOW_H
