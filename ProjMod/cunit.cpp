#include "cunit.h"




CUnit::CUnit(int damage, int health, int reach, int reloadT, Team team, bool alive)
{
    this->team = team;
    this->damage = damage;
    this->health = health;
    this->reach = reach;
    this->reloadT = reloadT;
    this->alive = alive;
}

CObject *CUnit::shoot(double xpos, double ypos, double xen, double yen, Team team, std::deque <CObject*> listOfObjects, double damage)
{
    CObject *obj;
    double a, b;
    BulletType  type = bullet;
    obj = addBullets(xpos,ypos,xen,yen,type,team,listOfObjects,damage);
    return obj;

}

PointnDist CUnit::checkEnemies(std::deque<CObject *> listOfObjects, double x, double y, Team team)
{
    double xp = 0 , yp = 0;
    double min = 9999;
    double akt = 0;
    bool flag = false;

    PointnDist nearestEnemyPosition;
    auto distance = [](double x1, double y1, double x2, double y2)->double{return sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));};
    for (CObject *obj : listOfObjects)
    {
        if(CUnit *unit = dynamic_cast <CUnit*>(obj))
        {
            if ((team != unit->getTeam())&&(unit->isAlive()==1))
            {
                akt = distance(obj->getPositionX(),obj->getPositionY(),x,y);
                if ((akt < min)&&(akt>0))
                {
                    min = akt;
                    xp = obj->getPositionX();
                    yp = obj->getPositionY();
                    nearestEnemyPosition.x=xp;
                    nearestEnemyPosition.y=yp;
                    nearestEnemyPosition.dist=min;
                    flag = true;

                }
            }
        }

    }
    if (flag == false){
        nearestEnemyPosition.x=x;
        nearestEnemyPosition.y=y;
        nearestEnemyPosition.dist=2000;}

    return nearestEnemyPosition;
}

Team CUnit::getTeam()
{
    return team;
}

int CUnit::getDamage()
{
    return damage;
}

CObject *CUnit::addBullets(double xpos, double ypos, double xend, double yend, BulletType type, Team team, std::deque<CObject *> listOfObjects, double damage)
{
    CObject *bul;
    switch (type)
    {
    case bullet:
        bul = new CBullet(xpos,ypos,xend,yend,team,damage);
        break;
    }

    return bul;

}

bool CUnit::isAlive()
{
    return alive;
}

void CUnit::updatecyclesToFire()
{
    cyclesToFire++;
}

void CUnit::resetcyclesToFire()
{
    cyclesToFire = 0;
}

void CUnit::reduceHealth(double damage)
{
    health = health - damage;
    if (health <= 0){
        alive = 0;
    }
}

int CUnit::getcyclesToFire()
{
    return cyclesToFire;
}



