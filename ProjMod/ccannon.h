#ifndef CCANNON_H
#define CCANNON_H
#include "cstationaryunit.h"

/**
 * @brief The CCannon class klasa opisujaca nieruchome dzialo
 */
class CCannon: public CStationaryUnit
{
public:
    /**
     * @brief CCannon konstruktor obiektu dzialo
     * @param team druzyna jednostki
     * @param xpos wspolrzedna x jednostki na mapie
     * @param ypos wspolrzedna y jednostki na mapie
     * @param area promien okregu wyznaczajacego pole powierchni jednstki
     * @param damage obrazenia zadawane przz jednostke
     * @param health punkty zycia jednostki
     * @param reach zasieg pola razenia jednostki
     * @param reloadT czas przeladowania jednostki
     * @param alive pole, ktorego wartosc stwierdza czy jednostka ciagle istnieje na mapie
     */
    CCannon(Team team, double xpos, double ypos,double area = 50,int damage = 100,int health = 1000, int reach = 1000, int reloadT= 1000, bool alive = 1);

    /**
     * @brief update polimorficzna metoda modyfiklujaca parametry danego obiektu
     * oraz mogaca zwrocic nowy obiekt
     * @param listOfObjects lista obiektow na mapie
     * @return wskaznik do obiektu
     */
    CObject *update(std::deque <CObject*> listOfObjects) override;
};

#endif // CCANNON_H

