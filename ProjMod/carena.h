#ifndef CARENA_H
#define CARENA_H
#include "structures.h"
#include <deque>
#include <cobject.h>
#include <cjeep.h>
#include <ctank.h>
#include <csoldier.h>
#include <ccannon.h>
#include "cbullet.h"

/**
 * @brief Klasa przechowujaca liste obiektow na mapie oraz zarzadzajaca symulacja
 */


class CArena
{
public:
    /**
     * Konstruktor klasy CArena
     */
    CArena();

    /**
     * @brief addObjects metoda dodajaca obiekty do listy obiektow
     * @param X wspolrzedna X polozenia obiektu na mapie gry
     * @param Y wspolrzedna X polozenia obiektu na mapie gry
     * @param type typ dodawanej jednostki
     * @param team druzyna dodawanej jednostki
     * @return wskaznik do obiektu
     */
    CObject *addObjects(double X, double Y, UnitType type, Team team);

    /**
     * @brief getObjectsList metoda pobierajaca liste obiektow
     * @return lista obiektow
     */
    std::deque<CObject*> getObjectsList();

    /**
     * @brief Update metoda aktualizujaca liste obiektow
     */
    void Update();

    /**
     * @brief startWar metoda ustawiajaca wartosc pola simulation na 1
     */
    void startWar();

    /**
     * @brief stopWar metoda ustawiajaca wartosc pola simulation na 0
     */
    void stopWar();


protected:
    /**
     * @brief cleanObjectsList metoda usuwajaca niepotrzebne pociski z mapy
     */
    void cleanObjectsList();
    std::deque<CObject*> objectsList; ///< lista obiektow
    bool simulation = 0; ///< pole sterujace wykonywaniem symulacji

private:



};

#endif // CARENA_H
