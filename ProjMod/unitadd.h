#ifndef UNITADD_H
#define UNITADD_H
#include "carena.h"
#include <QDialog>
#include "structures.h"
#include "sfmlcanvas.h"
#include <cstdlib>
#include <stdlib.h>
#include <time.h>

/**
 * formularz dodania jednostki
 */

namespace Ui {
class UnitAdd;
}

class UnitAdd : public QDialog
{
    Q_OBJECT

public:
    explicit UnitAdd(QWidget *parent = 0);
    ~UnitAdd();

    /**
     * @brief setArenaPointer metoda ustawiajaca wskaznik na arene na podany
     * @param wsk podany wskaznik na arene
     */
    void setArenaPointer(CArena *wsk);

    /**
     * @brief setCanvasPointer metoda ustawiajaca wskaznik na SFMLCanvas na podany
     * @param wsk podany wskaznik na SFMLCanvas
     */
    void setCanvasPointer(SfmlCanvas *wsk);

private slots:
    /**
     * @brief on_addButton_clicked metoda dodajaca jednostke danego typu
     */
    void on_addButton_clicked();

    /**
     * @brief on_redRadioButton_clicked metoda wybierajaca czerwona frakcje
     */
    void on_redRadioButton_clicked();

    /**
     * @brief on_blueRradioButton_2_clicked metoda wybierajaca niebieska frakcje
     */
    void on_blueRradioButton_2_clicked();

    /**
     * @brief on_generateButton_clicked metoda generujaca losowa pozycje na mapie
     */
    void on_generateButton_clicked();

private:
    Ui::UnitAdd *ui;
    CArena *arena; ///< wskaznik na arene
    SfmlCanvas *canvas;///< wskaznik na SFMLCanvas
    UnitType unitType;///< pole typu jednostki
    Team team;///< pooe druzyny


};

#endif // UNITADD_H
