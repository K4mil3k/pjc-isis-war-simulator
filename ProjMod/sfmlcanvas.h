#ifndef SFMLCANVAS_H
#define SFMLCANVAS_H

#include "sfmlwidget.h"
#include <iostream>
#include <deque>
#include "cmoving.h"
#include "carena.h"
#include "cobject.h"
#include "cbullet.h"
#include <cmath>



/**
 * @brief The SfmlCanvas class klasa zarzadzajaca sprite'ami oraz lista rysowanych obiektow
 */

class SfmlCanvas : public SfmlWidget
{
public:
    SfmlCanvas(QWidget *Parent = 0);

    void onInit() override;
    void onUpdate() override;
    /**
     * @brief addUnits metoda dodajaca obiekt do listy rysowanych obiektow
     * @param object wskaznik na dodawany obiekt
     */
    void addUnits(CObject *object);

    /**
     * @brief setArenaPointer metoda ustawiajaca wskaznik na arene na dany
     * @param wsk dany wskaznik
     */
    void setArenaPointer(CArena *wsk);

    /**
     * @brief refreshlistOfObjectsCanvas metoda odwiezajaca liste rysowanych obiektow lista
     * podana przez silnik gry
     * @param listOfObjectsCanvas
     */
    void refreshlistOfObjectsCanvas(std::deque<CObject*> listOfObjectsCanvas);

private:
    sf::Texture m_tex;
    CArena *arena;
    sf::Texture background; ///< textura tla
    sf::Texture fireTexture; ///< textura ognia
    sf::Texture jeepTextureRED; ///< textura jeep'a czerwonego
    sf::Texture bulletTexture; ///< textura pocisku
    sf::Texture jeepTextureBLUE; ///< textura jeep'a niebieskiego
    sf::Texture tankTextureBLUE; ///< textura czolgu niebieskiego
    sf::Texture tankTextureRED; ///< textura czolgu czerwonego
    sf::Texture cannonTextureBLUE; ///< textura dziala niebeiskiego
    sf::Texture cannonTextureRED; ///< textura dziala czerwonoego
    sf::Sprite m_sprite; ///< rysowany sprite
    sf::Text text;
    sf::Font font;
    std::deque<CObject*> listOfObjectsCanvas; ///< lista obiektow rysowanych na mapie
    double angle = 0; ///< kat obrotu aktualny
    double angleprev = 0; ///< kat obrotu poprzedni
};

#endif // SFMLCANVAS_H
